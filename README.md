# async

This package provides the `clusterAsync` function, a variant of
`clusterCall` that returns once a specified number of nodes have
returned results. Jobs that are no longer needed are canceled by
sending their nodes a `SIGINT` signal. For now this requires that all
workers are running on `"localhist"`.

For now, loading this package loads parallel and does a _monkey patch_
on `parallel` to suspend interrupts around the work loop and to
discard interrupts that arrive unless they are explicitly caught. The
implementation of `clusterAsync` catches interrupts that occur when
calling the work function. This package has to be installed where the
workers will find it.

> Using a _monkey patch_ is only a temporary expedient and is not
> something that should usually be done or encouraged. Its use here in
> _not_ en endorsement of this approach. Once the framework has had
> some testing it may make sense to incorporate the small changes into
> the `parallel` package. At that time the _monkey patch_ can be
> dropped.

A simple example:

```r
library(async)
cl <- makePSOCKcluster(3)
pids <- clusterCall(cl, Sys.getpid)
clusterExport(cl, "pids")

## get first result when one jon finishes quickly, two take longer
clusterAsync(cl, 1,
             function() if (Sys.getpid() == pids[[1]]) 1 else Sys.sleep(10))

## get first result when all finish quickly
clusterAsync(cl, 1, function() 1)
```

There is still one race condition in this implementation: if a node
receives a termination signal _before_ it starts processing its work
function then the signal will be lost and the work function will run
to completion. This is not likely to be an issue in practice.
